# kera-e2clab-examples

[E2Clab](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/e2clab) is a framework that implements a rigorous methodology providing guidelines to move from real-life application workflows to representative settings of the underlying physical infrastructure. The goal is to accurately reproduce its relevant behaviors and therefore understand end-to-end performance.

These scripts allow to deploy KerA examples on the [Grid5000](https://www.grid5000.fr/w/Grid5000:Home) testbed using E2Clab.

## Installation

Start by installing [E2Clab](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/e2clab) on the frontend node by following [these instructions](https://kerdata.gitlabpages.inria.fr/Kerdata-Codes/e2clab/getting-started.html#installation).

Clone the current repository to get the examples:

```bash
git clone https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera-e2clab-examples.git
```

## Deployment

Before running an experiment, make sure to build the required images and artifacts.

### Preparing KerA and Flink images

The [KerA repository](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera.git) comes with Dockerfiles to build Docker images in a reproducible way. These images should be saved to tar archives for easy distribution (this can be done on your local machine):

```bash
git clone https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera.git
cd kera
docker build . --tag kera
docker save -o <path>/kera-docker.tar kera
docker build -f docker/flink/Dockerfile . --tag kerflink
docker save -o <path>kerflink-docker.tar kerflink
```

For them to be deployed on remote nodes, the `kera-docker.tar` and `kerflink-docker.tar` images must be placed in the `~/kera-e2clab-examples/artifacts/docker/` directory, on the frontend node.

### Preparing artifacts

If your experiment require one or multiples artifacts, place them inside `~/kera-e2clab-examples/artifacts/`. This directory is common to all experiments.

## Running an experiment

### Pick one

This repository contains many experiments.

| Name | Artifacts in `e2clab-kera-examples/artifacts` | Images in `e2clab-kera-examples/artifacts/docker` | Description |
|---|---|---|---|
| flink-consumer-1 | `BenchSyntheticKeraRichConsStreaming.jar` from [kera-flink-examples repository](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera-flink-examples) | `kera-docker.tar`, `kerflink-docker.tar` | Complete Flink consumer with best performance. Run your own producer. |
| flink-consumer-2 | `BenchSyntheticKeraPushRichConsStreaming.jar` from [kera-flink-examples repository](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera-flink-examples) | `kera-docker.tar`, `kerflink-docker.tar` | Complete Flink consumer with best performance + Plasma. Run your own producer. |
| kera-producer-1-flink-consumer-1 | `BenchSyntheticKeraRichConsStreaming.jar` from [kera-flink-examples repository](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera-flink-examples) | `kera-docker.tar`, `kera-producer-1.tar`, `kerflink-docker.tar` | Simple KerA producer ↔ complete Flink consumer with best performance. |
| kera-producer-1-flink-consumer-2 | `BenchSyntheticKeraPushRichConsStreaming.jar` from [kera-flink-examples repository](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera-flink-examples) | `kera-docker.tar`, `kera-producer-1.tar`, `kerflink-docker.tar` | Simple KerA producer ↔ complete Flink consumer with best performance + Plasma. |

If the experiment you pick implies Flink, you can display the Flink dashboard on your local machine:

```bash
ssh -NL 8081:<jobmanager_hostname>:8081 access.grid5000.fr 
```

The Flink dashboard will be available at `localhost:8081`.

### Commands

To run an experiment, please refer to the [E2Clab documentation](https://kerdata.gitlabpages.inria.fr/Kerdata-Codes/e2clab/getting-started.html#usage).

#### Layers & services

```bash
e2clab layers-services ~/kera-e2clab-examples/<name> ~/kera-e2clab-examples/artifacts/
```

#### Workflow

```bash
e2clab workflow ~/kera-e2clab-examples/<name> prepare   # prepare workflow (i.e. copy images and libraries, create a Kafka topic, etc.)
e2clab workflow ~/kera-e2clab-examples/<name> launch    # execute workflow (i.e. submit the Flink job)
e2clab workflow ~/kera-e2clab-examples/<name> finalize  # backup workflow data (i.e. data generated from processes that compose the workflow)
```
